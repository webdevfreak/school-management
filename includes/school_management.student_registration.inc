<?php

/**
 * @file
 * school_management.student_registration.inc
 */

/**
 * Form constructor for Student Registration.
 */
function school_management_student_registration($form, &$form_state) {

  // Generate form.
  $form['first_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Student first name*'),
    '#size' => '20',
    '#description' => t(''),
  );
  $form['last_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Student last name*'),
    '#size' => '20',
    '#description' => t('')
  );
  $form['dob'] = array(
    '#type' => 'textfield',
    '#title' => t('Student date of birth*'),
    '#size' => '20',
    '#description' => t('(dd/mm/yyyy) (e.g 15/06/1995)')
  );
  $gender_values = array(
    '' => t('--Select--'),
    'Boy' => t('Boy'),
    'Girl' => t('Girl')
  );
  $form['gender'] = array(
    '#title' => t('Student gender*'),
    '#type' => 'select',
    '#description' => '',
    '#options' => $gender_values,
  );
  $form['father_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Father`s name*'),
    '#size' => '20',
    '#description' => t('')
  );
  $form['father_mobile_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Father`s mobile number*'),
    '#size' => '20',
    '#description' => t('Starts with 0 i.e 07500000000')
  );
  $form['mother_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Mother`s name*'),
    '#size' => '20',
    '#description' => t('')
  );
  $form['mother_mobile_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Mother`s mobile number*'),
    '#size' => '20',
    '#description' => t('Starts with 0 i.e 07500000000')
  );
  $form['home_phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Home telephone*'),
    '#size' => '20',
    '#description' => t('Starts with 0 i.e 02000000000')
  );
  $form['parent_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Parent email*'),
    '#size' => '20',
    '#description' => t('')
  );
  $form['parent_address_line_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Parent/Student address line 1*'),
    '#size' => '40',
    '#description' => t('')
  );
  $form['parent_address_line_2'] = array(
    '#type' => 'textfield',
    '#title' => t('Parent/Student address line 2'),
    '#size' => '40',
    '#description' => t('')
  );
  $form['parent_borough'] = array(
    '#type' => 'textfield',
    '#title' => t('Parent/Student borough*'),
    '#size' => '20',
    '#description' => t('')
  );
  $form['parent_postcode'] = array(
    '#type' => 'textfield',
    '#title' => t('Parent/Student postcode*'),
    '#size' => '20',
    '#description' => t('')
  );
  $form['emergency_contact_name_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Emergency contact name 1*'),
    '#size' => '20',
    '#description' => t('()')
  );
  $form['emergency_contact_number_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Emergency contact number 1*'),
    '#size' => '20',
    '#description' => t('(Telephone or Mobile - Please ensure this is not parent`s contact number)'),
  );
  $form['emergency_contact_name_2'] = array(
    '#type' => 'textfield',
    '#title' => t('Emergency contact name 2*'),
    '#size' => '20',
    '#description' => t('()')
  );
  $form['emergency_contact_number_2'] = array(
    '#type' => 'textfield',
    '#title' => t('Emergency contact number 2*'),
    '#size' => '20',
    '#description' => t('(Telephone or Mobile - Please ensure this is not parent`s contact number)'),
  );
  $form['student_school_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Student school*'),
    '#size' => '20',
    '#description' => t('')
  );
  $form['student_school_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Student school address*'),
    '#size' => '40',
    '#description' => t('')
  );
  $form['language_spoken_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Language spoken 1*'),
    '#size' => '20',
    '#description' => t('')
  );
  $form['language_spoken_2'] = array(
    '#type' => 'textfield',
    '#title' => t('Language spoken 2'),
    '#size' => '20',
    '#description' => t('')
  );
  $form['allergies_medication'] = array(
    '#type' => 'textarea',
    '#title' => t('Allergies and medication (please list all)*'),
    '#default_value' => '-NA-',
    '#description' => t('')
  );
  $form['comments'] = array(
    '#type' => 'textarea',
    '#title' => t('Any other information you would like to share with us'),
    '#description' => t('')
  );
  $path = 'rules-regulations-student';
  $terms_url = l('rules and regulations', $path, array('attributes' => array('target' => '_blank')));
  $form['terms'] = array(
    '#type' => 'checkbox',
    '#title' => t('I have read the ' . $terms_url . ' of the School and agreed to abide by them.'),
    '#description' => t('')
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Register')
  );

  // Return configured form data.
  return $form;
}

/**
 * Form submission handler for school_management_student_registration().
 */
function school_management_student_registration_submit($form, &$form_state) {
  // Set today's date.
  $today = date('M-d-Y');

  // Unique reference.
  $dob = filter_xss($form_state['values']['dob']);
  $unique_reference = md5($dob . strtotime('now'));

  // Save in the database table.
  $query = db_insert('school_management_students')
    ->fields(array(
      'unique_reference' => $unique_reference,
      'status' => 'In Review',
      'class_type' => 'Weekend',
      'class' => '',
      'first_name' => filter_xss($form_state['values']['first_name']),
      'last_name' => filter_xss($form_state['values']['last_name']),
      'dob' => filter_xss($form_state['values']['dob']),
      'gender' => filter_xss($form_state['values']['gender']),
      'father_name' => filter_xss($form_state['values']['father_name']),
      'father_mobile_number' => filter_xss($form_state['values']['father_mobile_number']),
      'mother_name' => filter_xss($form_state['values']['mother_name']),
      'mother_mobile_number' => filter_xss($form_state['values']['mother_mobile_number']),
      'home_phone' => filter_xss($form_state['values']['home_phone']),
      'parent_email' => filter_xss($form_state['values']['parent_email']),
      'parent_address_line_1' => filter_xss($form_state['values']['parent_address_line_1']),
      'parent_address_line_2' => filter_xss($form_state['values']['parent_address_line_2']),
      'parent_borough' => filter_xss($form_state['values']['parent_borough']),
      'parent_postcode' => filter_xss($form_state['values']['parent_postcode']),
      'emergency_contact_name_1' => filter_xss($form_state['values']['emergency_contact_name_1']),
      'emergency_contact_number_1' => filter_xss($form_state['values']['emergency_contact_number_1']),
      'emergency_contact_name_2' => filter_xss($form_state['values']['emergency_contact_name_2']),
      'emergency_contact_number_2' => filter_xss($form_state['values']['emergency_contact_number_2']),
      'student_school_name' => filter_xss($form_state['values']['student_school_name']),
      'student_school_address' => filter_xss($form_state['values']['student_school_address']),
      'language_spoken_1' => filter_xss($form_state['values']['language_spoken_1']),
      'language_spoken_2' => filter_xss($form_state['values']['language_spoken_2']),
      'allergies_medication' => filter_xss($form_state['values']['allergies_medication']),
      'comments' => filter_xss($form_state['values']['comments']),
      'confirmed' => 0,
      'confirmed_date' => NULL,
      'deleted' => 0,
      'deleted_date' => NULL,
      'created' => REQUEST_TIME,
    ))
    ->execute();

  // Send email to parent with unique reference.
  $student_name = filter_xss($form_state['values']['first_name']) . ' ' . filter_xss($form_state['values']['last_name']);
  $to = filter_xss($form_state['values']['parent_email']);
  if ($to) {
    $subject = 'Admission application status';
    $message = 'Dear ' . filter_xss($form_state['values']['father_name']) . '

Thanks for submitting admission application for ' . $student_name . '.

You can go to our website (http://www.my-school-management.com/school/student-application-status) and use this reference number ' . $unique_reference . ' to check the application status.

We will send you notification once admission is confirmed.

Regards,
My School Management';
    school_management_mail('default_from', $to, $subject, $message);
  }

  // Display success message.
  drupal_set_message('Student registration form successfully submitted.');

  // Redirect.
  $form_state['redirect'] = array('/student-registration-thanks');
}

/**
 * Form validation handler for school_management_student_registration().
 */
function school_management_student_registration_validate($form, $form_state) {
  if (empty($form_state['values']['first_name']))
    form_set_error('first_name', 'Student First Name cannot be empty');

  if (empty($form_state['values']['last_name']))
    form_set_error('last_name', 'Student Last Name cannot be empty');

  if (empty($form_state['values']['dob']))
    form_set_error('dob', 'Student date of birth cannot be empty');

  if (empty($form_state['values']['gender']))
    form_set_error('gender', 'Student gender cannot be empty');

  if (empty($form_state['values']['father_name']))
    form_set_error('father_name', 'Father name cannot be empty');

  if (empty($form_state['values']['father_mobile_number']))
    form_set_error('father_mobile_number', 'Father`s mobile number cannot be empty');

  if ($form_state['values']['father_mobile_number'] != '') {
    if (!is_numeric($form_state['values']['father_mobile_number']))
      form_set_error('father_mobile_number', 'Father`s mobile number must be a number');
  }

  if (empty($form_state['values']['mother_name']))
    form_set_error('mother_name', 'Mother name cannot be empty');

  if (empty($form_state['values']['mother_mobile_number']))
    form_set_error('mother_mobile_number', 'Mother`s mobile number cannot be empty');

  if ($form_state['values']['mother_mobile_number'] != '') {
    if (!is_numeric($form_state['values']['mother_mobile_number']))
      form_set_error('mother_mobile_number', 'Mother`s mobile number must be a number');
  }

  if (empty($form_state['values']['home_phone']))
    form_set_error('home_phone', 'Home telephone cannot be empty');

  if ($form_state['values']['home_phone'] != '') {
    if (!is_numeric($form_state['values']['home_phone']))
      form_set_error('home_phone', 'Home telephone must be a number');
  }

  if (empty($form_state['values']['parent_email']))
    form_set_error('parent_email', 'Parent email cannot be empty');

  if ($form_state['values']['parent_email'] != '') {
    if (filter_var($form_state['values']['parent_email'], FILTER_VALIDATE_EMAIL) == false)
      form_set_error('parent_email', 'Parent email is not valid');
  }

  if (empty($form_state['values']['parent_address_line_1']))
    form_set_error('parent_address_line_1', 'Parent/Student address line 1 cannot be empty');

  if (empty($form_state['values']['parent_borough']))
    form_set_error('parent_borough', 'Parent/Student borough cannot be empty');

  if (empty($form_state['values']['parent_postcode']))
    form_set_error('parent_postcode', 'Parent/Student postcode cannot be empty');

  if (empty($form_state['values']['emergency_contact_name_1']))
    form_set_error('emergency_contact_name_1', 'Emergency contact name 1 cannot be empty');

  if (empty($form_state['values']['emergency_contact_number_1']))
    form_set_error('emergency_contact_number_1', 'Emergency contact number 1 cannot be empty');

  if ($form_state['values']['emergency_contact_number_1'] != '') {
    if (!is_numeric($form_state['values']['emergency_contact_number_1']))
      form_set_error('emergency_contact_number_1', 'Emergency contact number 1 must be a number');
  }

  if (empty($form_state['values']['emergency_contact_name_2']))
    form_set_error('emergency_contact_name_2', 'Emergency contact name 2 cannot be empty');

  if (empty($form_state['values']['emergency_contact_number_2']))
    form_set_error('emergency_contact_number_2', 'Emergency contact number 2 cannot be empty');

  if ($form_state['values']['emergency_contact_number_2'] != '') {
    if (!is_numeric($form_state['values']['emergency_contact_number_2']))
      form_set_error('emergency_contact_number_2', 'Emergency contact number 2 must be a number');
  }

  if (empty($form_state['values']['student_school_name']))
    form_set_error('student_school_name', 'Student school cannot be empty');

  if (empty($form_state['values']['student_school_address']))
    form_set_error('student_school_address', 'Student school address cannot be empty');

  if (empty($form_state['values']['language_spoken_1']))
    form_set_error('language_spoken_1', 'Language spoken 1 cannot be empty');

  if (empty($form_state['values']['allergies_medication']))
    form_set_error('allergies_medication', 'Allergies and medication cannot be empty');

  if ($form_state['values']['terms'] == '')
    form_set_error('terms', 'You must agree with the rules and regulations of the School');
}
