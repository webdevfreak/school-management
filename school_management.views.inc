<?php

/**
 * @file
 * school_management.views.inc
 */

/**
 * Implements hook_views_data_alter().
 *
 * @param $data
 *   Information about Views' tables and fields.
 */
function school_management_views_data_alter(&$data) {
  // Custom Views Form for Attendance.
  $data['school_management_students']['comments'] = array(
    'field' => array(
      'title' => t('Custom Views Form for Attendance'),
      'help' => t('Edit the attendance field'),
      'handler' => 'school_management_field_handler_attendance_edit',
    ),
  );
}

/**
 * Implements hook_views_data().
 */
function school_management_views_data() {
  // Define the base group of this table.
  $data['school_management_students']['table']['group'] = t('Student Registrations');

  // Define this as a base table – a table that can be described in itself by
  // views (and not just being brought in as a relationship).
  $data['school_management_students']['table']['base'] = array(
    'field' => 'id', // This is the identifier field for the view.
    'title' => t('Student Registrations'),
    'help' => t('Student Registrations table contains student data.'),
    'weight' => -10,
  );

  // Join
  $data['school_management_students']['table']['join'] = array(
    'school_management_student_attendance' => array(
      'left_field' => 'id',
      'field' => 'student_id',
    ),
  );

  // User ID table field.
  $data['school_management_students']['id'] = array(
    'title' => t('Member ID'),
    'help' => t('Member ID which is linked with users database table.'),
    'relationship' => array(
      'base' => 'school_management_student_attendance', // The name of the table to join with.
      'base field' => 'student_id', // The name of the field on the joined table.
      'handler' => 'views_handler_relationship',
      'label' => t('Default label for the relationship'),
      'title' => t('Title shown when adding the relationship'),
      'help' => t('More information on this relationship'),
    ),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Unique reference table field.
  $data['school_management_students']['unique_reference'] = array(
    'title' => t('Unique reference'),
    'help' => t('Unique reference.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Status table field.
  $data['school_management_students']['status'] = array(
    'title' => t('Status'),
    'help' => t('Status.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // Class Type table field.
  $data['school_management_students']['class_type'] = array(
    'title' => t('Class Type'),
    'help' => t('Class Type.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // Class table field.
  $data['school_management_students']['class'] = array(
    'title' => t('Class'),
    'help' => t('Class.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // First Name table field.
  $data['school_management_students']['first_name'] = array(
    'title' => t('First Name'),
    'help' => t('First Name.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // Last Name table field.
  $data['school_management_students']['last_name'] = array(
    'title' => t('Last Name'),
    'help' => t('Last Name.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // DOB table field.
  $data['school_management_students']['dob'] = array(
    'title' => t('Date Of Birth'),
    'help' => t('Date Of Birth.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // Gender table field.
  $data['school_management_students']['gender'] = array(
    'title' => t('Gender'),
    'help' => t('Gender.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // Parent Name table field.
  $data['school_management_students']['father_name'] = array(
    'title' => t('Father Name'),
    'help' => t('Father Name.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // Parent Mobile 1 table field.
  $data['school_management_students']['father_mobile_number'] = array(
    'title' => t('Father`s Mobile Number'),
    'help' => t('Father`s Mobile Number.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // Main Class Teacher table field.
  $data['school_management_students']['main_class_teacher'] = array(
    'title' => t('Main Class Teacher'),
    'help' => t('Main Class Teacher.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Book Class Teacher table field.
  $data['school_management_students']['book_class_teacher'] = array(
    'title' => t('Book Class Teacher'),
    'help' => t('Book Class Teacher.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  // Confirm Admission table field.
  $data['school_management_students']['confirmed'] = array(
    'title' => t('Confirm Admission'),
    'help' => t('Confirm Admission.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  // Deleted table field.
  $data['school_management_students']['deleted'] = array(
    'title' => t('Delete Student'),
    'help' => t('Delete Student.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  // Registration Date table field.
  $data['school_management_students']['created'] = array(
    'title' => t('Registration Date'),
    'help' => t('Registration Date.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // Teacher Registrations - Define the base group of this table.
  $data['school_management_teachers']['table']['group'] = t('Teacher Registrations');

  // Teacher Registrations - Define this as a base table – a table that can be
  // described in itself by views (and not just being brought in as a relationship).
  $data['school_management_teachers']['table']['base'] = array(
    'field' => 'id', // This is the identifier field for the view.
    'title' => t('Teacher Registrations'),
    'help' => t('Teacher Registrations table contains teacher data.'),
    'weight' => -10,
  );

  // Teacher Registrations - User ID table field.
  $data['school_management_teachers']['id'] = array(
    'title' => t('Teacher ID'),
    'help' => t('Teacher ID which is linked with users database table.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  // Teacher Registrations - Status table field.
  $data['school_management_teachers']['status'] = array(
    'title' => t('Status'),
    'help' => t('Status.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // Teacher Registrations - Username table field.
  $data['school_management_teachers']['user_name'] = array(
    'title' => t('Username'),
    'help' => t('Username.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // Teacher Registrations - Password table field.
  $data['school_management_teachers']['password'] = array(
    'title' => t('Password'),
    'help' => t('Password.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // Teacher Registrations - First Name table field.
  $data['school_management_teachers']['first_name'] = array(
    'title' => t('First Name'),
    'help' => t('First Name.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // Teacher Registrations - Last Name table field.
  $data['school_management_teachers']['last_name'] = array(
    'title' => t('Last Name'),
    'help' => t('Last Name.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // Teacher Registrations - NI Number table field.
  $data['school_management_teachers']['ni_number'] = array(
    'title' => t('NI Number'),
    'help' => t('NI Number.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // Teacher Registrations - DOB table field.
  $data['school_management_teachers']['dob'] = array(
    'title' => t('Date Of Birth'),
    'help' => t('Date Of Birth.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // Teacher Registrations - Contact number 1 table field.
  $data['school_management_teachers']['contact_number_1'] = array(
    'title' => t('Contact number 1'),
    'help' => t('Contact number 1.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // Teacher Registrations - Deleted table field.
  $data['school_management_teachers']['deleted'] = array(
    'title' => t('Delete Teacher'),
    'help' => t('Delete Teacher.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE, // This is used by the table display plugin.
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_query_alter().
 *
 * @param $view
 *   Information about Views' tables and fields.
 *
 * @param $query
 *   Information about query used in this views.
 */
function school_management_views_query_alter(&$view, &$query) {
  // Only alter query for these views.
  if ($view->name == 'student_attendance' || $view->name == 'student_attendance') {
    // The Drupal user object.
    global $user;

    // Get teacher id based on the given logged in user id.
    $teacher_id = school_management_get_teacher_id($user->name);

    // Update contextual filters in following views.
    // Query logged in user id with school_management_teachers table
    // and get teacher id to be used in where condition.
    if ($view->name == 'student_attendance') {
      $query->where[0]['conditions']['0']['value'][':school_management_students_main_class_teacher'] = $teacher_id->id;
    }
    if ($view->name == 'student_attendance_book') {
      $query->where[0]['conditions']['0']['value'][':school_management_students_book_class_teacher'] = $teacher_id->id;
    }
  }
}
