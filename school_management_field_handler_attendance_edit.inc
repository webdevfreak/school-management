<?php

/**
 * @file
 * serra_invoice_field_handler_memo_edit.inc
 */

/**
 * A Views' field handler for editing memo.
 */
class school_management_field_handler_attendance_edit extends views_handler_field {

  function construct() {
    parent::construct();
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    // Render a Views form item placeholder. This causes Views to wrap
    // the View in a form. Render a Views form item placeholder.
    return '<!--form-item-' . $this->options['id'] . '--' . $this->view->row_index . '-->';
  }

  /**
   * Add to and alter the form created by Views.
   */
  function views_form(&$form, &$form_state) {

    // Create a container for our replacements.
    $form[$this->options['id']] = array(
      '#type' => 'container',
      '#tree' => TRUE,
    );

    // Iterate over the result and add our replacement fields to the form.
    foreach ($this->view->result as $row_index => $row) {

      // Initialise default value.
      $default_value = '';

      // The Drupal user object.
      global $user;

      // Get teacher id based on the current logged in user id.
      $teacher_id = school_management_get_teacher_id($user->name);
      $teacher_id = $teacher_id->id;

      // Get class type.
//      $class = school_management_get_class_type();
      // Student id.
      $student_id = $row->id;

      // Todays date.
      $date = date('Y-m-d');

      // Prepare query condition.
      $db_and = db_and();
      $db_and->condition('teacher_id', $teacher_id);
      $db_and->condition('student_id', $student_id);
//      $db_and->condition('class', $class);
      $db_and->condition('date', $date);

      // Run the query to get a default value if any.
      $query = db_select('school_management_students_attendance', 's')
        ->fields('s', array('id', 'attendance_status'))
        ->condition($db_and)
        ->execute()
        ->fetchObject();
      if ($query) {
        $default_value = $query->attendance_status;
      }

      // Configure form field and load default value if any.
      $attendance_values = array(
        '' => t('--Select--'),
        'Present' => t('Present'),
        'Absent' => t('Absent'),
        'Late' => t('Late'),
        'Sick' => t('Sick'),
      );
      $form[$this->options['id']][$row_index] = array(
        '#type' => 'select',
        '#options' => $attendance_values,
        '#default_value' => $default_value,
        '#element_validate' => array('view_form_school_management_field_handler_attendance_edit_validate'),
      );
    }
  }

  /**
   * Form submit method.
   */
  function views_form_submit($form, &$form_state) {

    // The Drupal user object.
    global $user;

    // Get teacher id based on the current logged in user id.
    $teacher_id = school_management_get_teacher_id($user->name);
    $teacher_id = $teacher_id->id;

    // Get class type.
//    $class = school_management_get_class_type();
    // Iterate over the view result.
    foreach ($this->view->result as $row_index => $row) {

      // Student id.
      $student_id = $row->id;

      // Grab the submitted form value.
      $attendance_status = $form_state['values'][$this->options['id']][$row_index];

      // Save posted values in table.
      $date = date('Y-m-d');
      $query = db_merge('school_management_students_attendance')
        ->key(array(
          'teacher_id' => $teacher_id,
          'student_id' => $student_id,
//          'class' => $class,
          'date' => $date,
        ))
        ->fields(array(
          'teacher_id' => $teacher_id,
          'student_id' => $student_id,
          'created' => REQUEST_TIME,
//          'class' => $class,
          'attendance_status' => $attendance_status,
          'date' => $date,
        ))
        ->execute();
    }

    // Display success message.
    drupal_set_message('Attendance successfully updated.');
  }

}

/**
 * Validation callback for the memo element.
 *
 * @param $element
 * @param $form_state
 */
function view_form_school_management_field_handler_attendance_edit_validate($element, &$form_state) {
  // @todo validate they entered anything.
}
